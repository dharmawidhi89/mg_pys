from pyspark.sql import SparkSession
from pyspark.sql.functions import col,explode
import sys

# cl_name = sys.argv[1]
cl_name="sample_analytics.customers"
unampas=""

def load_mg(name):
    mg_spark = SparkSession.builder \
        .appName("myApp") \
        .config(f"spark.mongodb.input.uri", f"mongodb+srv://{unampas}@cluster0.hmtlc.mongodb.net/{cl_name}") \
        .config("spark.mongodb.output.uri", f"mongodb+srv://{unampas}@cluster0.hmtlc.mongodb.net/{cl_name}") \
        .getOrCreate()
    df = mg_spark.read.format("mongo").load()
    df.createOrReplaceTempView("df_temp")
    dk = mg_spark.sql("select * from df_temp where cast(birthdate as date)>'1989-03-10'")

    return dk

def save_mg(df):
    df.write.format('bigquery') \
        .mode('overwrite') \
        .option('table', cl_name) \
        .option("temporaryGcsBucket", "dataproc-staging-us-central1-617940705268-w4m9ghiv") \
        .save()

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    data = load_mg(cl_name)
    print(f"Row to select {str(data.count())}")
    data.show(5,False)
    save_mg(data)
    print("process done")
