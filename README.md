# Mongo to BigQUery

For this Demo, Iam using Google Compute to as spark environment due to no Mongo DB environment connected to Cluster.
This Job will load data from MongoDB to stored it in BigQuery, for implementation in HDP will need some modification. 
### Prereq

```bash
wget https://gitlab.com/dharmawidhi89/mg_pys/-/raw/master/main.py
wget https://repo1.maven.org/maven2/org/mongodb/spark/mongo-spark-connector_2.11/2.4.2/mongo-spark-connector_2.11-2.4.2.jar
wget https://repo1.maven.org/maven2/org/mongodb/mongo-java-driver/3.12.1/mongo-java-driver-3.12.1.jar
wget https://repo1.maven.org/maven2/com/google/cloud/spark/spark-bigquery-with-dependencies_2.11/0.17.1/spark-bigquery-with-dependencies_2.11-0.17.1.jar

```



### How to run

```bash
gcloud dataproc jobs submit pyspark --cluster=min-cl main.py --region=us-central1 --jars=mongo-spark-connector_2.11-2.4.2.jar,mongo-java-driver-3.12.1.jar,spark-bigquery-with-dependencies_2.11-0.17.1.jar
 -- 1000
 ```

### Test Code in PySpark Env
```bash
pyspark --conf spark.jars=/home/dharmawidhi89/mongo-spark-connector_2.11-2.4.2.jar,mongo-java-driver-3.12.1.jar,spark-bigquery-with-dependencies_2.11-0.17.1.jar

```
